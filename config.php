<?php
/**
 * Config file for change of WordPress domain.
 *
 * This is supposed to be run as a complement of usual switch of domain,
 * using WordPress multisite, probably with domain mapping.
 *
 * Leafstudios takes no responsibility for using this code.
 *
 * @author Johannes Henrysson <johannes@leafstudios.se>, Leafstudios HB
 */

// Settings for database
$database = array(
	'host' => 'localhost',
	'database' => 'database',
	'user' => 'username',
	'password' => 'password',
);

// Settings for WordPress
$wordpress = array(
	'old_domain' => 'old.domain.com',
	'new_domain' => 'new.domain.com',
	'prefix' => 'wp_prefix',
);