<?php
/**
 * Script to change WordPress domain.
 *
 * @see config.php for more information.
 */
error_reporting(E_ALL);

require_once(dirname(__FILE__) . '/config.php');

/**
 * Connect to database, or die if fail.
 *
 * @param array $database
 */
function connect_to_database($database) {
	print ".";
	if (!mysql_connect($database['host'], $database['user'], $database['password'])) {
		die(mysql_error() . "\n");
	}
	print ".";
	if (!mysql_select_db($database['database'])) {
		die(mysql_error() . "\n");
	}
	print ".";
}

/**
 * Fix posts, almost like a little bootstrap.
 *
 * @param array $wordpress
 *   Wordpress settings, prefix, old_domain, new_domain.
 */
function fix_posts($wordpress) {
	extract($wordpress);
	
	print ".";
	$sql = "SELECT ID, post_content
		FROM {$prefix}_posts
		WHERE post_content LIKE '%{$old_domain}%'";
	$result = mysql_query($sql) or die(mysql_error() . "\n");
	while ($row = mysql_fetch_assoc($result)) {
		$row['post_content'] = stripslashes($row['post_content']);
		$row['post_content'] = str_replace($old_domain, $new_domain, $row['post_content']);
		
		update_post($prefix, $row);
	}
}

/**
 * Update an single post.
 *
 * @param string $prefix
 * @param array $data
 */
function update_post($prefix, $data) {
	print ".";
	$id = is_numeric($data['ID']) ? intval($data['ID']) : die("Missing id.\n");
	$content = !empty($data['post_content']) ?
		mysql_real_escape_string($data['post_content']) : die("Missing post_content.\n");
	
	$sql = "UPDATE {$prefix}_posts
		SET post_content='{$content}'
		WHERE ID='{$id}'";
	mysql_query($sql) or die(mysql_error() . "\n");
}

print "Connect to database.";
connect_to_database($database);
print "done!\n";

print "Update domain in posts.";
$posts = fix_posts($wordpress);
print "done!\n";
